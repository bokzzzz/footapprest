﻿using FootAppREST.Connection;
using FootAppREST.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootAppREST.DAO
{
    public class AnnouncementDAO
    {
        public List<Announcement> GetAnnouncements(String personType,DateTime date)
        {
            List<Announcement> announcments = new List<Announcement>();

            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from announcement where PersonType='" + personType + "' and `Release` >'" +date.ToString("yyyy-MM-dd HH:mm:ss")+"'    order by `Release` desc" ;
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                announcments.Add(new Announcement
                {
                    Release = reader.GetDateTime("Release"),
                    Text = reader.GetString("Text"),
                    Title = reader.GetString("Title"),
                    Position = reader.GetString("Position")
                });
            }
            reader.Close();
            return announcments;

        }
    }
}
