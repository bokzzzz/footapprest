﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootAppREST.Connection
{
    class DatabaseConnection
    {
        private static MySqlConnection MySqlConnection;
        private static DatabaseConnection Connection;

        private static string dbServer;
        private static string databaseName;
        private static string username;
        private static string password;
        
        static DatabaseConnection()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.Write(Directory.GetCurrentDirectory());
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            PropertiesReader propertiesReader = new PropertiesReader();
            propertiesReader.Load("database.properties");
            dbServer = propertiesReader.GetProperty("dbServer");
            databaseName = propertiesReader.GetProperty("database");
            username = propertiesReader.GetProperty("username");
            password = propertiesReader.GetProperty("password");
        }
        private DatabaseConnection()
        {
            Console.Write(Directory.GetCurrentDirectory());
                MySqlConnection = new MySqlConnection("Server=" +dbServer+ ";Database=" +databaseName+ ";Port=3306"+ ";UserId="+username + ";Password="+password);
        }
        public static DatabaseConnection GetConnection()
        {
            if (Connection == null)
            {
                Connection = new DatabaseConnection();
                MySqlConnection.Open();
            }
            return Connection;
        }
        public MySqlCommand CreateCommand()
        {
            return MySqlConnection.CreateCommand();
        }

        public void CloseConnection()
        {
            MySqlConnection.Close();
            MySqlConnection = null;
        }

    }
}
