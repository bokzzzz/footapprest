﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootAppREST.DTO
{
    public class Announcement
    {
        private String text;
        private DateTime release;
        private String title;
        private String position;
        public DateTime Release { get => release; set => release = value; }
        public string Text { get => text; set => text = value; }
        public string Title { get => title; set => title = value; }
        public string Position { get => position; set => position = value; }

        public Announcement()
        {
        }
        public Announcement(String text,DateTime release ,String position,String title)
        {
            this.text = text;
            this.release = release;
            this.title = title;
            this.position = position;
        }

    }
}
