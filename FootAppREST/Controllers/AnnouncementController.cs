﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using FootAppREST.DAO;
using FootAppREST.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace FootAppREST.Controllers
{
    [Route("footapp")]
    [ApiController]
    public class AnnouncementController : ControllerBase
    {
        [HttpGet("player/announcements/{date}")]
        [Produces(MediaTypeNames.Application.Json)]
        public Announcement[] GetAnnouncementsLastSeven(DateTime date)
        {
            Announcement[] announcments = new AnnouncementDAO().GetAnnouncements("Player",date).ToArray();
            return announcments;
        }
        [HttpGet("employee/announcements/{date}")]
        [Produces(MediaTypeNames.Application.Json)]
        public Announcement[] GetAnnouncementsForEmployees(DateTime date)
        {
            Announcement[] announcments = new AnnouncementDAO().GetAnnouncements("Employee", date).ToArray();

            return announcments;
        }
    }
}
