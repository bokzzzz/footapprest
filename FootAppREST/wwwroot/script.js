var date = new Date();
date.setDate(date.getDate() - 15);
let container = $("#announcement-panel");

function rest_client( target) {
    $.ajax({
        type: "GET",
        url: "/footapp/" + target + "/announcements/" + date.toUTCString()
    }).done(function (json) {
        
        container.empty();
        $.each(json, function (_idx, elem) {
            var card = $("<div>").addClass("card");
            var cardHeading = $("<a>").addClass("card-header").attr("id", "heading" + _idx).attr("data-toggle", "collapse").attr("data-target", "#collapse" + _idx).attr("aria-expanded", "false").attr("aria-controls", "collapse" + _idx).attr("href","#collapse");
            var button=$("<button>").addClass("btn btn-link collapsed").attr("type", "button").appendTo(cardHeading);
            cardHeading.appendTo(card);
            var collapse = $("<div>").addClass("collapse").attr("id", "collapse" + _idx).attr("aria-labelledby", "heading" + _idx);
            $("<div>").addClass("card-body").appendTo(collapse).text(elem.text);

            card.appendTo(container);
            collapse.appendTo(container);
            var badge = $("<h2>").appendTo(button);
            $("<span>")
                .addClass("badge badge-success mr-3 ")
                .text(elem.position).appendTo(badge);
            $("<strong>").text(elem.title).appendTo(badge);
            $("<h4>")
                .addClass("float-right").addClass("mt-3")
                .text(getTime(new Date(elem.release)))
                .appendTo(cardHeading);

        });
    });
}
function home() {
    container.empty();
}
function getTime(time){
    return time.getDate() + "." + (time.getMonth() + 1) + "." + time.getFullYear() + " " +
        time.getHours() + ":" + (time.getMinutes() < 10 ? '0' : '')+time.getMinutes();
}
function activeOnClick(id) {
    var current = document.getElementById("nav");
    var element = current.getElementsByClassName("active");

    for (var i = 0; i < element.length; i++)
        element[i].className = "";
    document.getElementById(id).setAttribute("class", "active");
   
}