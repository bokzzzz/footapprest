$('#bs4-slide-carousel').carousel({
    interval: 5000
});
var date = new Date();
date.setDate(date.getDate() - 7);
let container = $("#slide-to");
let container2 = $("#slider");
var boolean = true;
function rest_client() {
    $.ajax({
        type: "GET",
        url: "/footapp/employee/announcements/" + date.toUTCString()
    }).done(function (json) {

        date=new Date(new Date()-4000);
        $.each(json, function (_idx, elem) {
            $("<li data-target=\"#bs4-slide-carousel\" data-slide-to=\"" + _idx + "\" >")
                .appendTo(container);
            var newItem = $("<div class=\"carousel-item\">");
            var h2 = $("<h2>").appendTo(newItem);
            $("<span>")
                .addClass("badge badge-info mr-2 ")
                .text(elem.position).appendTo(h2);
            $("<strong>").text(elem.title).appendTo(h2);
            $("<h4>")
                .addClass("float-right")
                .text(new Date(elem.release).toLocaleString("bs-ba"))
                .appendTo(h2);
            $("<br>")
                .appendTo(newItem);
            $("<small>").addClass("display-4")
                .text(elem.text)
                .appendTo(newItem);
            $("<br><br>")
                .appendTo(newItem);
            newItem.appendTo(container2);
        });
        if (boolean) {
            container2.children().first().addClass("active");
            container.children().first().addClass("active");
            boolean = false;
        }
        
        });
}
setInterval(rest_client,  5000);

